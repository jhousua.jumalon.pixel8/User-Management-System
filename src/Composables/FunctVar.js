import { ref } from "vue";
import axios from "axios";
import {
  rows,
  dialogData,
  selectedRow,
  currentDialogForm,
  showEditDialog,
  showDeleteDialog,
} from "../pages/UserList";
import { form, currentForm } from "../pages/UserForm";

let tableData = ref([]);
let btnLoadingState = ref(false);
let resetValidation = ref(null);
let btnUserFormState = ref(true);
let btnAddressState = ref(true);
let btnCompanyState = ref(true);
let requiredInputState = ref(false);
let validateEmailState = ref(false);
let validateWebsiteState = ref(false);
let validateUsernameState = ref(false);
let validateLatLngState = ref(true);

const getUserData = () => {
  axios.get("https://jsonplaceholder.typicode.com/users").then((response) => {
    tableData.value = response.data.map((user) => ({
      id: user.id,
      name: user.name,
      username: user.username,
      email: user.email,
      address: `${user.address.street}, ${user.address.suite}, ${user.address.city} (${user.address.zipcode})`,
      latitude: user.address.geo.lat,
      longitude: user.address.geo.lng,
      phone: user.phone,
      website: user.website,
      company: user.company.name,
      catchPhrase: user.company.catchPhrase,
      bs: user.company.bs,
    }));
    rows.value = tableData.value;
  });
};

getUserData();

const toCapitalize = (words) => {
  const word = words.toLowerCase().split(" ");

  for (var i = 0; i < word.length; i++) {
    word[i] = word[i].charAt(0).toUpperCase() + word[i].slice(1);
  }

  const combinedWord = word.join(" ");
  return combinedWord;
};

const addUserData = () => {
  btnLoadingState.value = true;
  let tableLength = tableData.value.length;

  axios
    .post("https://jsonplaceholder.typicode.com/users", form.value)
    .then((response) => {
      if (response.status === 201) {
        tableData.value.push({
          id: ++tableLength,
          name: toCapitalize(response.data.name),
          username: response.data.username,
          email: response.data.email,
          address: toCapitalize(
            `${response.data.address.street}, ${response.data.address.suite}, ${response.data.address.city} (${response.data.address.zipcode})`
          ),
          latitude: response.data.address.geo.lat,
          longitude: response.data.address.geo.lng,
          phone: response.data.phone,
          website: response.data.website,
          company: response.data.company.name,
          catchPhrase: response.data.company.catchPhrase,
          bs: response.data.company.bs,
        });

        form.value.name = null;
        form.value.username = null;
        form.value.email = null;
        form.value.address.street = null;
        form.value.address.suite = null;
        form.value.address.city = null;
        form.value.address.zipcode = null;
        form.value.address.geo.lat = null;
        form.value.address.geo.lng = null;
        form.value.phone = null;
        form.value.website = null;
        form.value.company.name = null;
        form.value.company.catchPhrase = null;
        form.value.company.bs = null;
        currentForm.value = 1;
        resetValidation.value.reset();
      }
      btnLoadingState.value = false;
    });
};

const updateData = () => {
  btnLoadingState.value = true;
  axios
    .put(
      `https://jsonplaceholder.typicode.com/users/${selectedRow.value.id}`,
      dialogData.value
    )
    .then((response) => {
      if (response.status === 200) {
        let index = tableData.value.findIndex(
          (row) => row.id === selectedRow.value.id
        );

        tableData.value[index].name = toCapitalize(response.data.name);
        tableData.value[index].username = response.data.username;
        tableData.value[index].email = response.data.email;
        tableData.value[index].address = toCapitalize(
          `${response.data.address.street}, ${response.data.address.suite}, ${response.data.address.city} (${response.data.address.zipcode})`
        );
        tableData.value[index].latitude = response.data.address.geo.lat;
        tableData.value[index].longitude = response.data.address.geo.lng;
        tableData.value[index].phone = response.data.phone;
        tableData.value[index].website = response.data.website;
        tableData.value[index].company = response.data.company.name;
        tableData.value[index].catchPhrase = response.data.company.catchPhrase;
        tableData.value[index].bs = response.data.company.bs;

        selectedRow.value = {};
        currentDialogForm.value = 1;
      }
      showEditDialog.value = false;
      btnLoadingState.value = false;
    });
};

let deleteBtnLoadingState = ref(false);
const deleteData = () => {
  deleteBtnLoadingState.value = true;
  axios
    .delete(
      `https://jsonplaceholder.typicode.com/users/${selectedRow.value.id}`
    )
    .then((response) => {
      if (response.status === 200) {
        let index = tableData.value.findIndex(
          (row) => row.id === selectedRow.value.id
        );
        index !== -1 && tableData.value.splice(index, 1);
      }
      showDeleteDialog.value = false;
      deleteBtnLoadingState.value = false;
    });
};

const userFormValidation = () => {
  if (
    requiredInputState.value == true &&
    validateEmailState.value == true &&
    validateWebsiteState.value == true &&
    validateUsernameState.value == true
  ) {
    btnUserFormState.value = false;
  } else {
    btnUserFormState.value = true;
  }
};

const addressValidation = () => {
  if (requiredInputState.value == true && validateLatLngState.value == true) {
    btnAddressState.value = false;
  } else {
    btnAddressState.value = true;
  }
};

const companyValidation = () => {
  if (requiredInputState.value == true) {
    btnCompanyState.value = false;
  } else {
    btnCompanyState.value = true;
  }
};

const requiredInput = () => (val) => {
  if (!!val) {
    requiredInputState.value = true;
    userFormValidation();
    addressValidation();
    companyValidation();
    return true;
  } else {
    requiredInputState.value = false;
    userFormValidation();
    addressValidation();
    companyValidation();
    return "Field is required";
  }
};
const validateEmail = () => (val) => {
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(val)) {
    validateEmailState.value = true;
    userFormValidation();
    return true;
  } else {
    validateEmailState.value = false;
    userFormValidation();
    return "Invalid email address";
  }
};
const validateWebsite = () => (val) => {
  if (
    /^(?:(?:(?:[a-zA-z\-]+)\:\/{1,3})?(?:[a-zA-Z0-9])(?:[a-zA-Z0-9\-\.]){1,61}(?:\.[a-zA-Z]{2,})+|\[(?:(?:(?:[a-fA-F0-9]){1,4})(?::(?:[a-fA-F0-9]){1,4}){7}|::1|::)\]|(?:(?:[0-9]{1,3})(?:\.[0-9]{1,3}){3}))(?:\:[0-9]{1,5})?$/.test(
      val
    )
  ) {
    userFormValidation();
    validateWebsiteState.value = true;
    return true;
  } else {
    userFormValidation();
    validateWebsiteState.value = false;
    return "Invalid website domain";
  }
};

const validateUsername = () => (val) => {
  const res = /[a-zA-Z0-9_\.]$/.exec(val);
  if (!!res) {
    validateUsernameState.value = true;
    userFormValidation();
    return true;
  } else {
    validateUsernameState.value = false;
    userFormValidation();
    alert(
      "Usernames can only have: \n" +
        "- Lowercase and Uppercase Letters \n" +
        "- Numbers (0-9) \n" +
        "- Dots (.) \n" +
        "- Underscores (_)"
    );
    return false;
  }
};

const validateLatLng = () => (val) => {
  if (val.length < 16) {
    validateLatLngState.value = true;
    addressValidation();
    return true;
  } else {
    validateLatLngState.value = false;
    addressValidation();
    return "Maximum digit: 15";
  }
};

export {
  tableData,
  getUserData,
  addUserData,
  updateData,
  deleteData,
  resetValidation,
  requiredInput,
  validateEmail,
  validateWebsite,
  validateUsername,
  userFormValidation,
  btnUserFormState,
  btnAddressState,
  btnCompanyState,
  deleteBtnLoadingState,
  btnLoadingState,
  validateLatLng,
};
